import fastify from 'fastify'

import { env } from './env/index.js'
import { userRoutes } from './routes/user.js'

const app = fastify({logger: env.NODE_ENV === 'development'? true : false})

app.register(userRoutes, { prefix: 'user'})

app.listen({ port: Number(env.API_PORT)}, (err, address) => {
  if (err) {
    fastify.log.error(err)
    process.exit((1))
  }
})
