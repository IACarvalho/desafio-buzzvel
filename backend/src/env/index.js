import 'dotenv/config'
import { z } from 'zod'

const envSchema = z.object({
  NODE_ENV: z.enum(['development', 'test', 'production']).default('production'),
  API_PORT: z.string().default('3333')
})

const _env = envSchema.safeParse(process.env)

if(_env.success === false) {
  console.error('Invalid environment variables!', _env.error.format() )

  process.exit(1)
}

export const env = _env.data
