import setupKnex from 'knex'
import { env } from './env/index.js'

export const config = {
  client: 'mysql',
  connection: {
    host: 'localhost',
    port: 3306,
    user: 'iacarvalho',
    password: '123456',
    database: 'database'
  },
  migrations: {
    directory: './src/migrations'
  }
}

export const knex = setupKnex(config)
