import { randomUUID } from 'node:crypto'
import { z } from 'zod'

import { knex } from '../database.js'

export async function userRoutes(app) {

  app.get('/', async (request, reply) => {
    const users = await knex('users').select('*')

    return reply.send({users})
  })
}
